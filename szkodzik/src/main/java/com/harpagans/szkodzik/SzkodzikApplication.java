package com.harpagans.szkodzik;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SzkodzikApplication {

	public static void main(String[] args) {
		SpringApplication.run(SzkodzikApplication.class, args);
	}
}
